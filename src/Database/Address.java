package Database;

import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "Address")
public class Address extends Entity implements Serializable{
	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int addressId;
	private int countryId;
	private int regionId;
	private String city;
	private String street;
	private int houseNumber;
	private int localNumer;
	private String zipCode;
	private int typeId;
	@OneToOne
	@JoinColumn(name="personId")
	private Person person;
	
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}
	public int getLocalNumer() {
		return localNumer;
	}
	public void setLocalNumer(int localNumer) {
		this.localNumer = localNumer;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public int gettypeId() {
		return typeId;
	}
	public void setTyptypeId(int typtypeId) {
		typeId = typtypeId;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
}
