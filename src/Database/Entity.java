package Database;

public abstract class Entity {

	private int id;
	private EntityState state;
	
	public int getId() {
		return id;
	}
	public EntityState getState() {
		return state;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setState(EntityState state) {
		this.state = state;
	}

	
}
