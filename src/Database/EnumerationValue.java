package Database;

import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "EnumerationValue")
public class EnumerationValue extends Entity implements Serializable{
	
	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int intkey;
	private String stringKey;
	private String value;
	private String enumerationName;
	
	
	public int getIntkey() {
		return intkey;
	}
	public String getStringKey() {
		return stringKey;
	}
	public String getValue() {
		return value;
	}
	public String getEnumerationName() {
		return enumerationName;
	}
	public void setIntkey(int intkey) {
		this.intkey = intkey;
	}
	public void setStringKey(String stringKey) {
		this.stringKey = stringKey;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}
}
