package Database;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;




@javax.persistence.Entity(name = "Person")
public class Person extends Entity implements Serializable{
	@Column(name="personId", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int personId;
	private String firstName;
	private String surname;
	private String pesel;
	private String nip;
	private String email;
	private Date dateOfBirth;
@OneToOne	
@JoinColumn(name="userId")
	private User user;

@OneToMany	
@JoinColumn(name="addressId")
	private List<Address> address;
@JoinColumn(name="numberPhoneId")
	private List<NumberPhone> numberPhone;
	
	
	public int getPersonId() {
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getSurname() {
		return surname;
	}
	public String getPesel() {
		return pesel;
	}
	public String getNip() {
		return nip;
	}
	public String getEmail() {
		return email;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		if (this.equals(getUser()))
			user.getPerson();
		this.user = user;
	}
	
	
	public List<Address> getAddress() {
		return address;
	}
	public void setAddress(List<Address> address) {
		this.address = address;
	}
	public List<NumberPhone> getNumberPhone() {
		return numberPhone;
	}
	public void setNumberPhone(List<NumberPhone> numberPhone) {
		this.numberPhone = numberPhone;
	}
	

}
