package Database;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@javax.persistence.Entity(name = "RolesPermissions")
public class RolesPermissions extends Entity implements Serializable{
	
	private int roleId;
	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int permissionId;
	@OneToMany
	@JoinColumn(name="userRolesId")
	private List<UserRoles> userRoles;

	public int getRoleId() {
		return roleId;
	}
	public int getPermissionId() {
		return permissionId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}
	
	
}
