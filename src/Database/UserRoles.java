package Database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

public class UserRoles extends Entity implements Serializable{
	
	@Column(name="userId", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	@OneToMany
	private List<RolesPermissions> rolePermissions;
	@OneToMany
	@JoinColumn(name="userId")
	private List<User> users;
	
	public int getUserId() {
		return userId;

	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public List<RolesPermissions> getRolePermissions() {
		return rolePermissions;
	}
	public void setRolePermissions(List<RolesPermissions> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}

	
	
}
