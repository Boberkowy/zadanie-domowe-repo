package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.Entity;

public interface EntityBuilder<TEntity extends Entity>  {
	
	 public TEntity build(ResultSet rs) throws SQLException;
		
	}

