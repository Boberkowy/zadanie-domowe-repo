package Repository;

import java.util.List;

import Database.EnumerationValue;

public interface EnumerationValueRepository extends iRepository<EnumerationValue> {

		public List<EnumerationValue> withName(String name);
		public List<EnumerationValue> withintKey(int key, String name);
		public List<EnumerationValue> withStringKey(String key, String name);
}
