package Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Database.Entity;
import Database.EnumerationValue;
import Database.RolesPermissions;
import Database.User;
import Database.UserRoles;
import UnitOfWork.UnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class HsqlEnumerationValuesRepository implements EnumerationValueRepository, iUnitOfWorkRepository  {
	
	private Connection connection;
	private Statement statement;
	private UnitOfWork unitOfWork;
	protected PreparedStatement addEnumStmt;
	protected PreparedStatement delEnumStmt;
	protected PreparedStatement getUserStmt;
	protected PreparedStatement countEnumStmt;
	protected PreparedStatement updateEnumStmt;
	protected PreparedStatement roleUserStmt;
	protected PreparedStatement permissionUserStmt;
	
	public  HsqlEnumerationValuesRepository(Connection connection, UnitOfWork unitOfWork) {
		this.connection = connection;
		this.unitOfWork = unitOfWork;
		try {

			addEnumStmt = connection.prepareStatement("INSERT INTO User (Login, Password) VALUES (?, ?);");
			delEnumStmt = connection.prepareStatement("DELETE FROM User;");
			getUserStmt = connection.prepareStatement("SELECT * FROM User;");
			countEnumStmt = connection.prepareStatement("SELECT COUNT(*) FROM User;");
			updateEnumStmt = connection.prepareStatement("UPDATE Users SET (Login, Password)=(?,?) WHERE id=?;");
			roleUserStmt = connection.prepareStatement("INSERT INTO Role (UserId, RoleId) VALUES (?, ?);");
			permissionUserStmt = connection.prepareStatement("INSERT INTO Permission (RoleId, PermissionId) VALUES (?, ?);");

		} catch (SQLException e) {
			e.printStackTrace();
			
		}
	}

	public EnumerationValue withId(int id) {
		EnumerationValue enumerationValue = new EnumerationValue();
			try {
	            ResultSet result = statement.executeQuery("SELECT * FROM User WHERE Id = " + id + ";");
	            enumerationValue.setIntkey(result.getInt("IntKey"));
	            enumerationValue.setStringKey(result.getString("StringKey"));
	            enumerationValue.setValue(result.getString("Value"));
	            enumerationValue.setEnumerationName(result.getString("EnumerationName"));
	        } catch (SQLException e) {
	            e.printStackTrace();
	          
	        }
		return enumerationValue;
	}

	public List<EnumerationValue> allOnPage(PagingInfo page) {
		List<EnumerationValue> enumList = new ArrayList<EnumerationValue>();
		try{
			ResultSet result = statement.executeQuery("SELECT * FROM EnumerationValue;");
			while(result.next()){
				EnumerationValue enumer = new EnumerationValue();
				enumer.setIntkey(result.getInt("IntKey"));
				enumer.setStringKey(result.getString("StringKey"));
				enumer.setValue(result.getString("Value"));
				enumer.setEnumerationName(result.getString("EnumerationName"));
				enumList.add(enumer);
			}
			int count = 0;
			page.setTotalCount(count);
			page.setPage(1);
			}
			catch(SQLException e){
				e.printStackTrace();
			}
		return enumList;
	}

	
	public void add(EnumerationValue entity) {
			try{
				addEnumStmt.setInt(1, entity.getIntkey());
				addEnumStmt.setString(2, entity.getStringKey());
				addEnumStmt.setString(3, entity.getValue());
				addEnumStmt.setString(4, entity.getEnumerationName());
				addEnumStmt.executeQuery();
				
			}catch (SQLException e) {
	            e.printStackTrace();
			}
	}

	public void delete(EnumerationValue entity) {
		try{
			delEnumStmt.executeUpdate();
			
		} catch (SQLException e){
			e.printStackTrace();
		}
	}

	public void modify(EnumerationValue entity) {
		try{
			updateEnumStmt.setInt(1, entity.getIntkey());
			updateEnumStmt.setString(2, entity.getStringKey());
			updateEnumStmt.setString(3, entity.getValue());
			updateEnumStmt.setString(4, entity.getEnumerationName());
			updateEnumStmt.executeQuery();
		} catch(SQLException e){
			e.printStackTrace();
		}
	}

	public int count() {
		int count = 0;	
		try{
				count = countEnumStmt.executeUpdate();
			}	
				catch (SQLException e){
					e.printStackTrace();
			}
		return count;
	}



	public List<EnumerationValue> withName(String name) {
		List<EnumerationValue> enumList = new ArrayList<EnumerationValue>();
		try{
			ResultSet result  = statement.executeQuery("SELECT * FROM EnumerationValue WHERE EnumerationName = " +name+");");
			while(result.next()){
				EnumerationValue enumer = new EnumerationValue();
				enumer.setIntkey(result.getInt("IntKey"));
				enumer.setStringKey(result.getString("StringKey"));
				enumer.setValue(result.getString("Value"));
				enumer.setEnumerationName(result.getString("EnumerationName"));
				enumList.add(enumer);
			}
		}catch (SQLException e ){
			e.printStackTrace();
			
		}
		return enumList;
	}

	public List<EnumerationValue> withintKey(int key, String name) {
		List<EnumerationValue> enumList = new ArrayList<EnumerationValue>();
		try{
			ResultSet result = statement.executeQuery("SELECT * FROM EnumerationValue where IntKey = " + key + "AND EnumerationName =" + name + ";");
			while(result.next()){
			EnumerationValue enumer = new EnumerationValue();
			enumer.setIntkey(result.getInt("IntKey"));
			enumer.setStringKey(result.getString("StringKey"));
			enumer.setValue(result.getString("Value"));
			enumer.setEnumerationName(result.getString("EnumerationName"));
			enumList.add(enumer);
		}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return enumList;
	}


	public List<EnumerationValue> withStringKey(String key, String name) {
		List<EnumerationValue> enumList = new ArrayList<EnumerationValue>();
		try{
			ResultSet result = statement.executeQuery("SELECT * FROM EnumerationValue where StringKey = " + key + "AND EnumerationName =" + name + ";");
			while(result.next()){
			EnumerationValue enumer = new EnumerationValue();
			enumer.setIntkey(result.getInt("IntKey"));
			enumer.setStringKey(result.getString("StringKey"));
			enumer.setValue(result.getString("Value"));
			enumer.setEnumerationName(result.getString("EnumerationName"));
			enumList.add(enumer);
		}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return enumList;
	}

	public List<EnumerationValue> withId(EnumerationValue entity) {
		// TODO Auto-generated method stub
		return null;
	}

	public void persistAdd(Entity entity) {
		unitOfWork.markAsNew(entity, this);
		
	}

	public void persistDelete(Entity entity) {
		unitOfWork.markAsDelete(entity, this);			
	}

	public void persistUpdate(Entity entity) {
		unitOfWork.markAsChanged(entity,this);		
	}

	

	
	

	
}

	
	