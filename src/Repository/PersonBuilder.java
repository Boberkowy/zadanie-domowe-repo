package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.Person;

/*
 * 
 * 
 *
 * 
 */

public class PersonBuilder implements iEntityBuilder<Person>  {

	public Person build(ResultSet rs) throws SQLException {
		Person result = new Person();
		result.setFirstName(rs.getString("firstName"));
		result.setSurname(rs.getString("surname"));
		result.setPesel(rs.getString("Pesel"));
		result.setNip(rs.getString("Nip"));
		result.setEmail(rs.getString("Email"));
		result.setDateOfBirth(rs.getDate("DateOfBirth"));
		result.setId(rs.getInt("id"));

		return result;
	
	}

}
