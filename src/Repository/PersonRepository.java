
package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import Database.Person;
import UnitOfWork.iUnitOfWork;

public class PersonRepository  extends Repository<Person> implements iRepository<Person>{

	protected PersonRepository(Connection connection, iEntityBuilder<Person> builder, iUnitOfWork unitofwork) {
		super(connection, builder, unitofwork);
		
	}

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		update.setString(1,entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getPesel());
		update.setString(4, entity.getNip());
		update.setString(5, entity.getEmail());
		update.setDate(6, (Date) entity.getDateOfBirth());
		update.setInt(7, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
	
		insert.setString(1, entity.getFirstName());
		insert.setString(1, entity.getSurname());
		insert.setString(1, entity.getPesel());
		insert.setString(1, entity.getNip());
		insert.setString(1, entity.getEmail());
		insert.setDate(1, (Date) entity.getDateOfBirth());

	
	}

	@Override
	protected String getTableName() {
		
		return "Persons";
	}

	@Override
	protected String getUpdateQuery() {
		
		return "UPDATE Person SET(name,surname,pesel,nip,email,dateofbirth)=(?,?,?,?,?,?) WHERE id = ?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO Persons (name,,surname,pesel,nip,email,dateofbirth) VALUES (?,?,?,?,?,?) ";
	
	

}

	public List<Person> withId(Person entity) {
		// TODO Auto-generated method stub
		return null;

	}
}
