package Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Database.Entity;
import Database.EnumerationValue;
import Database.User;
import UnitOfWork.UnitOfWork;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;
import Repository.iRepository;

public abstract class Repository<TEntity extends Entity>  implements iRepository<TEntity>, iUnitOfWorkRepository {

	protected iUnitOfWork unitofwork;
	protected Connection connection;
	protected PreparedStatement selectByID;
	protected PreparedStatement insert;
	protected PreparedStatement delete;
	protected PreparedStatement update;
	protected PreparedStatement selectAll;
	protected PreparedStatement getUserStmt;
	protected PreparedStatement selectWithId;
	protected PreparedStatement count;
	protected iEntityBuilder<TEntity> builder;
	
	protected String selectByIDSql =  "SELECT * FROM " + getTableName() + "WHERE id =?";
	protected String deleteSql = "DELETE * FROM " + getTableName() + "WHERE id = ?";
	protected String selectAllSql= "SELECT * FROM " + getTableName();
	protected String getUserStmtSql = "SELECT * FROM" + getTableName();
	protected String totalSql = "SELECT COUNT(*) AS Total FROM " + getTableName()+";";
	protected Repository(Connection connection,
			iEntityBuilder<TEntity> builder, iUnitOfWork unitofwork){
		
		this.unitofwork= unitofwork;
		this.builder=builder;
		this.connection = connection;
		try {
			selectByID=connection.prepareStatement(selectByIDSql);
			insert = connection.prepareStatement(getInsertQuery());
			delete = connection.prepareStatement(deleteSql);
			update = connection.prepareStatement(getUpdateQuery());
			selectAll = connection.prepareStatement(selectAllSql);
			count = connection.prepareStatement(totalSql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<TEntity> allOnPage(PagingInfo page){
		List<TEntity> result = new ArrayList<TEntity>();
		try {
			ResultSet rs= selectAll.executeQuery();
			while(rs.next())
			{
				result.add(builder.build(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}       
	
	public List<TEntity> withId(){
		List<TEntity> result = new ArrayList<TEntity>();
		try{
			ResultSet rs = selectWithId.executeQuery();
			while(rs.next()){
				result.add(builder.build(rs));
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return result;
	}
	
	public void add(TEntity entity){
			unitofwork.markAsNew(entity, this);

	}
	public void delete(TEntity entity){
		 
		unitofwork.markAsDelete(entity, this);	
	}
		 
	public void modify (TEntity entity){
	
		unitofwork.markAsChanged(entity,this);
	}

	

	public void persistAdd(Entity entity){
		try {
			setUpInsertQuery((TEntity)entity);
			insert.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void persistUpdate(Entity entity){
		try {
			setUpUpdateQuery((TEntity)entity);
			update.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public void persistDelete(Entity entity) {

		try {
			delete.setInt(1, entity.getId());
			delete.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	
	protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
		
}
