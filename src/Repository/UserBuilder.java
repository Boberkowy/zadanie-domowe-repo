package Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.User;

public class UserBuilder implements iEntityBuilder<User> {

	public User build(ResultSet rs) throws SQLException {
		User result = new User();
		result.setId(rs.getInt("id"));
		result.setLogin(rs.getString("login"));
		result.setPassword(rs.getString("password"));
		return result;
	}

}
