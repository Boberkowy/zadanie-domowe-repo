package Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Database.Entity;
import Database.RolesPermissions;
import Database.User;
import Database.UserRoles;
import UnitOfWork.iUnitOfWork;

public class UserRepository extends Repository<User> implements iRepository<User> {

	protected PreparedStatement withLoginPass;
	protected PreparedStatement userRole;
	
	protected String withLoginPassSql = "SELECT * FROM User WHERE login = ? AND password = ?";
	protected String userRoleSql = "INSERT INTO Role (UserId, RoleId) VALUES (?, ?);";
	public UserRepository(Connection connection, iEntityBuilder<User> builder, iUnitOfWork unitofwork) {
		super(connection,builder, unitofwork);
			try {	
				withLoginPass = connection.prepareStatement(withLoginPassSql);
				userRole = connection.prepareStatement(userRoleSql);
			}
				catch (SQLException e){
					e.printStackTrace();
				}
			
	}

	@Override
	protected void setUpUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
		update.setString(2, entity.getPassword());
		update.setInt(3, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(User entity) throws SQLException {
		insert.setString(1, entity.getLogin());
		insert.setString(2, entity.getPassword());
	}

	@Override
	protected String getTableName() {
		
		return "users";
	}

	@Override
	protected String getUpdateQuery() {
		
		return "UPDATE users SET (login,password)=(?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		
		return  "INSERT INTO users(login,password)"
				+ "VALUES(?,?)";
	}

	public List<User> withId(User entity) {
		List<User> result = new ArrayList<User>();
		try{
			ResultSet rs = selectWithId.executeQuery();
			while(rs.next()){
				result.add(builder.build(rs));
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		return result;
	}


	
		
	}


	
