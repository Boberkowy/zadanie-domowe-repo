package Repository;

import java.util.List;



public interface iRepository <TEntity>{

	public List<TEntity> withId(TEntity entity);
	public List<TEntity> allOnPage(PagingInfo page);
	public void add(TEntity entity);
	public void delete(TEntity entity);
	public void modify (TEntity entity);

}
