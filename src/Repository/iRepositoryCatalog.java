package Repository;

public interface iRepositoryCatalog {
	
	public EnumerationValueRepository enumerations();
	public iUserRepository users();
}
