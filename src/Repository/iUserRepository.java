package Repository;

import java.util.List;

import Database.RolesPermissions;
import Database.User;
import Database.UserRoles;

public interface iUserRepository extends iRepository<User>{

	public List <User> withLogin(String login);
	public List <User> withLoginAndPassword(String login, String password);
	public void setupPermissions(UserRoles user, RolesPermissions role);
	
}
