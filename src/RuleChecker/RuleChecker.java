package RuleChecker;

import java.util.ArrayList;
import java.util.List;

public class RuleChecker <TEntity>{

	private List<iCheckRule<TEntity>> rules;
		
		public List<CheckResult> check(TEntity entity){
			List<CheckResult> result = new ArrayList<CheckResult>();
			for (iCheckRule<TEntity> rule : rules){
				result.add(rule.checkRule(entity));
			}
			return result;
		
		
		}
		
		public List<iCheckRule<TEntity>> getRules() {
			return rules;
		}

		public void setRules(List<iCheckRule<TEntity>> rules) {
			this.rules = rules;
		}

}
