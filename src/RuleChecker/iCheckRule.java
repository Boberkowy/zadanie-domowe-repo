package RuleChecker;

public interface iCheckRule<TEntity> {
	public CheckResult checkRule(TEntity entity);
}
