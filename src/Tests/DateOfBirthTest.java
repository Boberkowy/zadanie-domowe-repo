package Tests;

import static org.junit.Assert.*;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.DateOfBirthRule;

public class DateOfBirthTest {
	
	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	DateOfBirthRule rule = new DateOfBirthRule();
	
	/*
	 * Rule contains methods which checks if date of birth is like in pesel, also check if date is proper
	 * i mean if month is between 1-12, day 1-31.
	 */
	@Test
	public void entered_13_as_month_return_error() {
		Person p = new Person();
		p.setPesel("95073001230");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		}

	@Test
	public void check_if_dateofbirth_is_like_in_pesel() {
		Person p = new Person();
		p.setPesel("95073001230");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		}

	}


