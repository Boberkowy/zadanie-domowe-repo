package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.emailRule;

public class EmailTest {
	emailRule rule = new emailRule();
	
	/*
	Rule contains regex which require lowercase or uppercase letter or number at start, it may be followed by dot "." , requires @ sign 
	then again 	followed by letter or number and then dot "." and minimum 2 letters without numbers.
		
	*/
	
	@Test
	public void entered_invalid_email_without_at_sign_then_error() {
		Person p= new Person();
		p.setEmail("qwerty.com");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	@Test
	public void entered_invalid_email_with_number_at_the_end_then_error() {
		Person p= new Person();
		p.setEmail("qwerty@qwerty.c0m");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void entered_valid_email(){
		Person p = new Person();
		p.setEmail("s12668@pjwstk.edu.pl");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}

}
