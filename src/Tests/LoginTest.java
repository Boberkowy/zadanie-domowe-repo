package Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Database.User;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.LoginRule;

public class LoginTest {

	LoginRule loginRule = new LoginRule();
	
	/*
	login may be only lowercase letter and eventually contains numbers, length at least 3 chars and maximum of 15	
		
	*/
	@Test
	public void entered_wrong_login_with_special_sign_then_error(){
		User user = new User();
		user.setLogin("qwerty!");
		CheckResult result = loginRule.checkRule(user);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void entered_login_with_uppercase_letter_then_error(){
		User user = new User();
		user.setLogin("QWERTY");
		CheckResult result = loginRule.checkRule(user);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void entered_too_short_or_too_long_login_then_error(){
		User user1 = new User();
		User user2 = new User();
		user1.setLogin("QW"); // too short
		user2.setLogin("qwertyqwertyasdzxc"); // too long
		CheckResult result1 = loginRule.checkRule(user1);
		CheckResult result2 = loginRule.checkRule(user2);
		assertTrue(result1.getResult().equals(RuleResult.Error));
		assertTrue(result2.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void entered_proper_login(){
		User user1 = new User();
		User user2 = new User();
		user1.setLogin("asdasdasd");
		user2.setLogin("qwerty123");
		CheckResult result1 = loginRule.checkRule(user1);
		CheckResult result2 = loginRule.checkRule(user2);
		assertTrue(result1.getResult().equals(RuleResult.Error));
		assertTrue(result2.getResult().equals(RuleResult.Error));
	}
}
