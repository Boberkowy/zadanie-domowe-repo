package Tests;

import static org.junit.Assert.*;
import rules.NameRule;
import org.junit.Test;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;

public class NameTest {

		NameRule nameRule = new NameRule();
		
	@Test
	public void check_if_name_is_not_null_then_error() {
	Person p = new Person();
	CheckResult result =nameRule.checkRule(p);
	assertTrue(result.getResult().equals(RuleResult.Error));
	
	}
		

	
	@Test 
	public void entered_nothing_then_error(){
		Person p = new Person();
		p.setFirstName("");
		CheckResult result = nameRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void entered_numbers_or_sign_then_error(){
		Person p1 = new Person();
		Person p2 = new Person();
		p1.setFirstName("123554");
		p2.setFirstName("#$%^&&*");
		CheckResult result1 = nameRule.checkRule(p1);
		CheckResult result2 = nameRule.checkRule(p2);
		assertTrue(result1.getResult().equals(RuleResult.Error));
		assertTrue(result2.getResult().equals(RuleResult.Error));
	}
	@Test
	public void if_proper_name_then_ok(){
		Person p = new Person();
		p.setFirstName("Mateusz");
		CheckResult result = nameRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
}
