	package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.NipRule;

public class NipTest {
	
	NipRule nipRule = new NipRule();
	/*
	NipRule contains methods which check if nip is not too long or too short and also checksum 
	which is required to make a nip number proper
		
	*/
	@Test
	public void check_if_nip_is_not_null_then_error() {
		Person p = new Person();
		p.setNip(null);
		CheckResult result = nipRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void check_if_nip_is_not_filled() {
		Person p = new Person();
		p.setNip("");
		CheckResult result = nipRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	

	@Test
	public void entered_too_short_nip_then_error() {
		Person p = new Person();
		p.setNip("123");
		CheckResult result = nipRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	} 
	@Test
	public void entered_too_long_nip_then_error() {
		Person p = new Person();
		p.setNip("123234543543543");
		CheckResult result = nipRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	} 
	
	@Test
	public void entered_invalid_nip_with_bad_checksum_then_error() {
		Person p = new Person();
		p.setNip("7121067253");
		CheckResult result = nipRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	
	@Test
	public void entered_valid_nip(){
		Person p = new Person();
		p.setNip("7121067254");
		CheckResult result = nipRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}

}
