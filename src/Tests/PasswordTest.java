package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Database.Person;
import Database.User;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.PasswordRule;

public class PasswordTest {

	PasswordRule passRule = new PasswordRule();
	
	/*
	password MUST contains uppercase and lowercase letter. rest is optional , length at least 5 chars and maximum of 20	
		
	*/

	@Test
	public void entered_wrong_pass_with_special_sign(){
		User user = new User();
		user.setPassword("asdqwe!@#");
		CheckResult result = passRule.checkRule(user);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void entered_too_short_pass(){
		User user = new User();
		user.setPassword("asd");
		CheckResult result = passRule.checkRule(user);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	@Test
	public void entered_pass_without_uppercase_letter(){
		User user = new User();
		user.setPassword("qwerty1");
		CheckResult result = passRule.checkRule(user);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void entered_pass_without_lowercase_letter(){
		User user = new User();
		user.setPassword("QWERTY1");
		CheckResult result = passRule.checkRule(user);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	@Test
	public void entered_proper_pass(){
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		user1.setPassword("Asd123");
		user2.setPassword("123qwWERry");
	user3.setPassword("asdv32ZCzx");
		CheckResult result1 = passRule.checkRule(user1);
		CheckResult result2 = passRule.checkRule(user2);
		CheckResult result3 = passRule.checkRule(user3);
		assertTrue(result1.getResult().equals(RuleResult.Ok));
		assertTrue(result2.getResult().equals(RuleResult.Ok));
		assertTrue(result3.getResult().equals(RuleResult.Ok));
		
	}
}
