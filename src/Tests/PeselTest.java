package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.PeselRule;

public class PeselTest {
	
		PeselRule peselRule = new PeselRule();
	/*
	 * Rule contains methods which check if checksum is proper and if pesel has 11 numbers. 
	 */
	@Test
	public void check_if_pesel_is_null_then_error(){
		Person p = new Person();
		p.setPesel(null);
		CheckResult result = peselRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void entered_too_short_pesel_then_error(){
		Person p = new Person();
		p.setPesel("9507300");
		CheckResult result = peselRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	@Test
	public void entered_pesel_with_bad_checksum_then_error(){
		Person p = new Person();
		p.setPesel("95073001231");
		CheckResult result = peselRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}	
	@Test
	public void entered_too_long_pesel_then_error(){
		Person p = new Person();
		p.setPesel("950730234501231");
		CheckResult result = peselRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}	

	
	@Test
	public void entered_valid_pesel(){
		Person p = new Person();
		p.setPesel("95073001230"); 
		CheckResult result = peselRule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
}
