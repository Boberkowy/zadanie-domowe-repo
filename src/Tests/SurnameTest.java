package Tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import rules.NameRule;
import rules.surnameRule;

public class SurnameTest {
	
	surnameRule rule = new surnameRule();
	
	@Test
	public void check_if_name_is_not_null() {
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	@Test
	public void check_if_name_is_not_filled(){
		Person p = new Person();
		p.setSurname("");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	@Test
	public void if_proper_name_then_ok(){
		Person p = new Person();
		p.setSurname("Owerczuk");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
}
