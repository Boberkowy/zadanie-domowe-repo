package UnitOfWork;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import Database.Entity;
import Database.EntityState;

public class UnitOfWork implements iUnitOfWork{

	
		private Connection connection;

		private Map<Entity, iUnitOfWorkRepository> entities = 
				new LinkedHashMap<Entity, iUnitOfWorkRepository>();
		
				
		public UnitOfWork(Connection connection) {
					super();
					this.connection = connection;
					
					try {
						connection.setAutoCommit(false);
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				
		public void saveChanges() {
			for(Entity entity: entities.keySet())
			{
				switch(entity.getState())
				{
				case Modified:
					entities.get(entity).persistUpdate(entity);
					break;
				case Deleted:
					entities.get(entity).persistDelete(entity);
					break;
				case New:
					entities.get(entity).persistAdd(entity);
					break;
				case UnChanged:
					break;
				default:
					break;}
			}
			
			try {
				connection.commit();
				entities.clear();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		public void undo() {
			entities.clear();
			
		}

		
		public void markAsNew(Entity entity, iUnitOfWorkRepository repo) {
			entity.setState(EntityState.New);
			entities.put(entity, repo);
		}
		
		public void markAsDelete(Entity entity, iUnitOfWorkRepository repo) {
			entity.setState(EntityState.Deleted);
			entities.put(entity, repo);
		}

		
		public void markAsChanged(Entity entity, iUnitOfWorkRepository repo) {
			entity.setState(EntityState.Modified);
			entities.put(entity, repo);
			
		}

		
}
