package UnitOfWork;

import Database.Entity;

public interface iUnitOfWork {

	public void saveChanges();
	public void undo();
	public void markAsNew(Entity entity, iUnitOfWorkRepository repo);
	public void markAsDelete(Entity entity, iUnitOfWorkRepository repo);
	public void markAsChanged(Entity entity, iUnitOfWorkRepository repo);
}