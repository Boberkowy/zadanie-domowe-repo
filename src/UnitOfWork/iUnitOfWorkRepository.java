package UnitOfWork;

import Database.Entity;

public interface iUnitOfWorkRepository {

	public void persistAdd(Entity entity);
	public void persistDelete(Entity entity);
	public void persistUpdate(Entity entity);
}
