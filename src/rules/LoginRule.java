package rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Database.User;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import RuleChecker.iCheckRule;


public class LoginRule implements iCheckRule<User>{
	
	private Pattern pattern;
	private Matcher matcher;
	private static final String LOGIN_PATTERN = "^[a-z0-9_-]{3,15}$";
	
	public CheckResult checkRule(User entity) {
		if (Validate(entity.getLogin()) == false){
			return new CheckResult("",RuleResult.Error);
		}
		return new CheckResult("",RuleResult.Ok);
	}
	
	private boolean Validate(String login){
		pattern = Pattern.compile(LOGIN_PATTERN);
		matcher = pattern.matcher(login);
		return matcher.matches();
	}
}
