package rules;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import RuleChecker.iCheckRule;

public class NipRule implements iCheckRule<Person>{
	
		private byte NIP[] = new byte[10];
		
	public CheckResult checkRule(Person entity) {
		if(Validate(entity.getNip()) == false){
			return new CheckResult("",RuleResult.Error);
		}
		return new CheckResult("",RuleResult.Ok);
	}

	private boolean Validate(String nip){
		if (nip.length() != 10){
			return false;
		}
			if(checkNip(nip)){
				return true;
			}
			return false;
		}
	
	private boolean checkNip(String nip) {
		for (int i = 0; i < 10; i++) {
			NIP[i] = Byte.parseByte(nip.substring(i, i + 1));
		
			int sum;
		sum = 6 * NIP[0] +
		5 * NIP[1] +
		7 * NIP[2] +
		2 * NIP[3] +
		3 * NIP[4] +
		4 * NIP[5] +
		5 * NIP[6] +
		6 * NIP[7] +
		7 * NIP[8];
		sum %= 11;
		 
		if (NIP[9] == sum)
		return true;
		
		}  
		return false;
}
}
