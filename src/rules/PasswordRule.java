package rules;

import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import RuleChecker.iCheckRule;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import Database.User;

public class PasswordRule implements iCheckRule<User>{
	  
	  private Pattern pattern;
	  private Matcher matcher;
	  private static final String PASSWORD_PATTERN = 
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{5,20})";
	
	  public CheckResult checkRule(User entity) {
		  if(Validate(entity.getPassword()) == false){
			  return new CheckResult("",RuleResult.Error);
		  }
		  return new CheckResult("",RuleResult.Ok);
	}
	  private boolean Validate(String password){
			pattern = Pattern.compile(PASSWORD_PATTERN);
			matcher = pattern.matcher(password);
			return matcher.matches();
		}
	
}
