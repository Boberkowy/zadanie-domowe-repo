package rules;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import RuleChecker.iCheckRule;

public class PeselRule implements iCheckRule<Person> {
	private byte PESEL[] = new byte[11];


	public CheckResult checkRule(Person entity) {
		if(entity.getPesel()==null)
			return new CheckResult("",RuleResult.Error);
		if(entity.getPesel().length() != 11){
			return new CheckResult("",RuleResult.Error);
		}	
		if (checkPesel(entity.getPesel()) == false) {
				return new CheckResult("",RuleResult.Error);
			}
		
		return new CheckResult("",RuleResult.Ok);
	}

	
		 
		private boolean checkPesel(String pesel) {
			for (int i = 0; i < 11; i++) {
				PESEL[i] = Byte.parseByte(pesel.substring(i, i + 1));
			}
		int sum = 1 * PESEL[0] +
		3 * PESEL[1] +
		7 * PESEL[2] +
		9 * PESEL[3] +
		1 * PESEL[4] +
		3 * PESEL[5] +
		7 * PESEL[6] +
		9 * PESEL[7] +
		1 * PESEL[8] +
		3 * PESEL[9];
		sum %= 10;
		sum = 10 - sum;
		sum %= 10;
		 
		if (sum == PESEL[10]) {
		return true;
		}
		else {
		return false;
		}
		}
		 
	
		}
