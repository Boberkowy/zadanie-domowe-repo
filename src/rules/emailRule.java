package rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import RuleChecker.iCheckRule;

public class emailRule implements iCheckRule<Person>{
	private Pattern pattern;
	private Matcher matcher;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public CheckResult checkRule(Person entity) {
		if(Validate(entity.getEmail()) == false){	
			return new CheckResult("",RuleResult.Error);
		}
		return new CheckResult("",RuleResult.Ok);
	}
	
	private boolean Validate(String email){
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
