package rules;

import Database.Person;
import RuleChecker.CheckResult;
import RuleChecker.RuleResult;
import RuleChecker.iCheckRule;

public class surnameRule implements iCheckRule<Person>{

	public CheckResult checkRule(Person entity) {
		
		if(entity.getSurname().matches("[a-zA-z]+([ '-][a-zA-Z]+)*" )){
			return new CheckResult("", RuleResult.Ok);
		}
		else return new CheckResult("",RuleResult.Error);
		

}
}
